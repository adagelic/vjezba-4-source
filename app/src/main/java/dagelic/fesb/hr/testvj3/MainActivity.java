package dagelic.fesb.hr.testvj3;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DataStorage.fillData();

        ListView myListView = findViewById(R.id.myListView);
        MyAdapter myAdapter = new MyAdapter(getApplicationContext());

        myListView.setAdapter(myAdapter);
    }
}
