package dagelic.fesb.hr.testvj3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;

    public MyAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return DataStorage.listViewData.size();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
            view = mInflater.inflate(R.layout.list_view_single_item, viewGroup, false);

        final TextView name = (TextView) view.findViewById(R.id.name);
        final TextView owner = (TextView) view.findViewById(R.id.owner);
        final ImageView imageTmb = (ImageView)  view.findViewById(R.id.image_tmb);

        final Cat cat = DataStorage.listViewData.get(i);

        name.setText(cat.getName());
        owner.setText(cat.getOwner());
        imageTmb.setImageResource(cat.getTmbImageId(mContext));

        return view;
    }


    @Override
    public Object getItem(int i) { return null; }

    @Override
    public long getItemId(int i) { return 0; }

}
